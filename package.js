Package.describe({
  name: 'inspinia-scss-lib',
  version: '0.0.2',
  documentation: 'README.md',
});

// Npm.depends({
//   toastr: '2.1.2',
// });

Package.onUse(function (api) {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'fourseven:scss@4.5.4']);
  // summernote
  api.addFiles([
    'summernote/summernote.js',
    'summernote/summernote-ru-RU.js',
    'summernote/summernote.css',
  ], 'client');
  api.addAssets([
    'summernote/font/summernote.eot',
    'summernote/font/summernote.ttf',
    'summernote/font/summernote.woff',
  ], 'client');
  // toastr
  api.mainModule('inspinia-scss-lib.js', 'client');
});
